# Obligatorisk oppgave 3 - PowerShell pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 11.6.c (Prosesser og tr�der)
* 12.4.c (En prosess sin bruk av virtuelt og fysisk minne)
* 13.10.b (Internminne)
* 13.10.c (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer
* Leif Torbj�rn N�svold

## Sjekkliste

* Har navnene p� gruppemedlemmene blitt skrevet inn over? | GJORT
* Har l�ringsassistenter og foreleser blitt lagt til med leserettigheter? | GJORT
* Er issue-tracker aktivert? | GJORT
* Er pipeline aktivert, og returnerer pipelinen "Successful"? | GJORT
