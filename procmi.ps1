# procmi.ps1
# script som tar process-ID som argument og skriver VMSize + Str p� Working Set til fil PID-Date.meminfo

# for hvert argument som sendes: 
if ( $args[0] -eq "" ) { Write-Host "PID ikke inntastet, scriptet avsluttes..."; exit }
else {
    for ( $i = 0; $i -lt $args.count; $i++ ) {

        $id = $args[$i]
        $dato = Get-Date -Format "yyyyMMdd-HHmmss" # henter dato p� formatet yyyyMMdd-HHmmss
        $filnavn = "$id"+"--"+"$dato"+".meminfo" # setter sammen filnavn
        Write-Host "Filnavn: $filnavn"

        Set-Content -Encoding UTF8 -Path '$filnavn' -Value "******** Minneinfo om prosess med PID $id ********"

        # henter dataene:
        $VMSize = (Get-Process -id ($id)).VirtualMemorySize 
        $workingSet = (Get-Process -id ($id)).WorkingSet

        # lager variabel med dataene som skal settes inn
        $insert = "Total bruk av virtuelt minne (VMSize): $VMSize KB
        Storrelse paa working set: $workingSet KB"

        Add-Content -Encoding UTF8 -path "$filnavn" -Value "$insert"
    }
} 