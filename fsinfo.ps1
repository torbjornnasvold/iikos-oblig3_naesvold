<# fsinfo.ps1

Tar et dir som argument og gj�r f�lgende:
    - Finner hvor stor del av partisjonen angitt dir befinner seg p� som er full
    - Hvor mange filer som befinner seg i angitt dir
    - Gjennomsnittlig filst�rrelse
    - St�rste fil + full path til denne
#>
# tar f�rste dir som anigs:
$folder = Get-Item args[0]

# finner freespace og disksize ved hjelp av get ciminstance:
$disk = Get-CimInstance -ClassName win32_logicaldisk
$disk = $disk[0]
$partisjon = 100-(($disk.FreeSpace)/($disk.Size))
Write-Host "Partisjonen ($args[0]) befinner seg p� er $partisjon% full"

$files = Get-ChildItem $folder -Recurse | Where-Object { ! $_.PSIsContainer } | sort -Descending length # henter filst�rrelse sortert etter synkende str
$antFiles = $files.Count                                                                                # teller antall filer
Write-Host "Det finnes $antFiles filer"                                                                 # skriver ut

$lgFile = $files | Select-Object -First 1 | %{$_.FullName} | Out-String                                  # henter st�rste fil
Write-Host "Den st�rste er $lgFile"                                               

$lgFileSize = $files | Select-Object -first 1 | %{$_.Length} | Out-String                                # henter str p� st�rste fil
Write-Host "som er $lgFileSize B stor"

$avgFileSize = $files | Measure-Object -Average Length | Format-Table Average -HideTableHeaders | Out-String | %{$_.Replace(',',".")} # gjennomsnittlig filst�rrelse
Write-Host "Gjennomsnittlig filst�rrelse er $avgFileSize B"