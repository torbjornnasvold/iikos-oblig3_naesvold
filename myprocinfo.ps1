# pwershellscript som skriver informasjon om prosesser (se meny func for detaljer)

# skriver brukermenyen til brukeren
function meny() {
write-Host "
    1 - Hvem er jeg og hva er navnet p� dette scriptet?
    2 - Hvor lenge er det siden siste boot? 
    3 - Hvor mange prosesser og tr�der finnes? 
    4 - Hvor mange context switch'er fant sted siste sekund?
    5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og usermode siste sekund? 
    6 - Hvor mange interrupts fant sted siste sekund? 
    9 - Avslutt dette scriptet 
    Velg en funksjon: "
}


#funksjonen skriver tiden siden siste reboot (menyvalg 2):
function lastReboot() {
    $uptime = ((get-date) - (gcim Win32_OperatingSystem).LastBootUpTime)
    $dager = $uptime.Days
    $timer = $uptime.Hours
    $minutter = $uptime.Minutes
    Write-Host "Det er $dager dager, $timer timer og $minutter minutter siden siste boot ble gjennomf�rt"
}

# funksjon som returnerer antall prosesser og tr�der som finnes (menyvalg 3)
function processes {
    $antProc = (get-CimInstance Win32_PerfFormattedData_PerfOS_System).Processes
    $antThread = (get-CimInstance Win32_PerfFormattedData_PerfOS_System).Threads
    Write-Host "Det finnes $antProc prosesser og $antThread tr�der"
}

# skriver antall context switcher siste sekund til skjerm (menyvalg 4)
function contextSwitch() {
    $antSwitch = (get-CimInstance Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPersec
    Write-Host "Det ble gjennomf�rt $antSwitch context switch'er siste sekund"
}

# funksjon som skriver tid i user/kernal mode siste sekund (menyvalg 5)
function userKernel() {
    $userTime = (get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).PercentUserTime
    $userTime = $userTime[0]                                                                              # velger f�rste objekt i array for siste 10 sekunder
    $kernelTime = (get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).PercentPrivilegedTime
    $kernelTime = $kernelTime[0]
    Write-Host "Prosenandel av tid siste sekund var fordelt p� $userTime % i usermode og $kernelTime % i kernelmode"
}

# funksjon som skriver antall interrupts siste sekund (menyvalg 6)
function interrupts() {
    $antInterrupts = (get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation).InterruptsPersec
    $antInterrupts = $antInterrupts[0]                                                                     # velger f�rste objekt i array for siste 10 sekunder
    Write-Host "Antall interrupts siste sekund var $antInterrupts" 
}

# do-while loopes til avbryt velges av bruker
do {
    meny;                      # skriver meny til brukeren
    $svar = read-Host          # henter svar fra brukeren og switcher p� svaret:

    switch ( $svar ) {
        1 { 
            $bruker = (get-CimInstance Win32_OperatingSystem).RegisteredUser
            $scriptname = $MyInvocation.MyCommand.Name
            Write-Host "Jeg er $bruker, og navnet p� dette scriptet er $scriptname" 
            break
        }

        2 { lastReboot; break }
        3 { processes; break }
        4 { contextSwitch; break }
        5 { userKernel; break }
        6 { interrupts; break }
        9 { Exit }
        default { Write-Host "Ugyldig menyvalg, vennligst pr�v igjen!" }
    }

} while ($svar -ne 9)
