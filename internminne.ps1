# interminne.ps1
# script som henter prosesser tilh�rende chrome og skriver ut navn, pid og internminne av disse

$id = Get-Process chrome # henter id

if ($id -eq "") { Write-Host "Fant ingen prosesser tilh�rende chrome"; exit } # feilemelding

# for hver prosess:
for ( $i=0; $i -lt $id.Count; $i++ ) {
    $idc = $id[$i]
    $idcp = $idc.id                         # henter ut id for prosess i 
    $idcpT = $idc.Threads.Count             # teller antall tr�der for prosess i 
    Write-Host "chrome $idcp $idcpT"        # skriver chrome, pid antall tr�der
}
